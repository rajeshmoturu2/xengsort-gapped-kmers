from xengsort.hash_3FCVbb import build_hash
from importlib import import_module
from xengsort.build import build_from_fasta, make_calc_lucky_bits, generate_kmer_iterator
import pytest
from xengsort.dnaio import fasta_reads

#get value set
vimport = "values.xenograft"
vmodule = import_module("xengsort."+vimport, __package__)
values = vmodule.initialize(3)  

# Build hash table
def build_ht(k, n, b, lb):
    h = build_hash(int(4**k), n, b,
            "random", 7, values.update,
            aligned=False, nfingerprints=-1,
            maxwalk=5000, lbits=lb)

    (total, failed, walkstats) = build_from_fasta(
            ["tests/data/test_luckybits.fa"], k, h, values.get_value_from_name_host,
            rcmode="max", walkseed=7, maxfailures=0)
    if failed:
        assert False, "Error in build_ht"
    print("# Calc lucky bits")
    calc_luckybits = make_calc_lucky_bits(h)
    calc_luckybits(h.hashtable)
    print("# Done: Calc lucky bits")
    return h

def check_luckybits(h, b, k, lb):
    get_sig_at = h.get_signature_at
    get_value_at = h.get_value_at
    get_luckybits_at = h.get_luckybits_at
    is_slot_empty_at = h.is_slot_empty_at
    get_key_choice_sig = h.get_key_choice_sig
    (h1, h2, h3) = h.get_pf
    table = h.hashtable
    if lb == 0: return
    for page in range(h.npages): # iterate over all pages
        for slot in range(b): # iterate over all slots
            if is_slot_empty_at(table, page, slot):
                continue
            sig = get_sig_at(table, page, slot)
            key, choice = get_key_choice_sig(page, sig)
            if choice == 0: continue
            if choice == 1:
                if get_luckybits_at(table, h1(key)[0]) & 1 != 1:
                    print(get_luckybits_at(table, h1(key)[0]), "1")
                    assert False, "Wrong lucky bit"
            elif choice == 2:
                if lb == 1:
                    if get_luckybits_at(table, h1(key)[0]) & 1 != 1:
                        print(get_luckybits_at(table, h1(key)[0]), "1")
                        assert False, "Wrong lucky bit"
                    if get_luckybits_at(table, h2(key)[0]) & 1 != 1:
                        print(get_luckybits_at(table, h2(key)[0]), "1")
                        assert False, "Wrong lucky bit"
                else:
                    if get_luckybits_at(table, h1(key)[0]) & 2 != 2:
                        print(get_luckybits_at(table, h1(key)[0]), "2")
                        assert False, "Wrong lucky bit"
                    if get_luckybits_at(table, h2(key)[0]) & 2 != 2:
                        print(get_luckybits_at(table, h2(key)[0]), "2")
                        assert False, "Wrong lucky bit"

def check_get_value(h, b, k, lb):
    k, kmers = generate_kmer_iterator(k, "max")
    h.get_value
    for seq in fasta_reads("tests/data/test_luckybits.fa", True):
        for kmer in kmers(seq, 0, len(seq)):
            value = get_value(h.hashtable, kmer)
            if value < 0:
                print(value)
                assert False, "Value not found"

@pytest.mark.parametrize("b", [2,3,4])
@pytest.mark.parametrize("k", [23,24,25])
@pytest.mark.parametrize("lb", [0,1,2])
def test_luckybits(b, k, lb):
    h = build_ht(k, 110000, b, lb)
    check_luckybits(h, b, k, lb)
    check_get_value(h, b, k, lb)
