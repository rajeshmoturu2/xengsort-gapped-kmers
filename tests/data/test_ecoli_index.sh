xengsort index ecoli.h5 --host <(zcat ecoli.fa.gz) --graft <(zcat ehec.fa.gz) -k 23 -n 6861013 -p 4 --fill 0.95 --chunkprefixlen 0 --shortcutbits 2 --threads 2

# Notes: The two E. coli genomes are from
# https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Escherichia_coli/reference/

# ecoli.fa.gz is the K-12 strain from
# https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Escherichia_coli/reference/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz

# ehec.fa.gz is the O157:H7 str. Sakai strain from
# https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Escherichia_coli/reference/GCF_000008865.2_ASM886v2/GCF_000008865.2_ASM886v2_genomic.fna.gz
